const dest = require('gulp-dest');
const gulp = require('gulp');

const yaml = require('js-yaml');
const fs   = require('fs');

var twig = require('gulp-twig');
var shell = require('shelljs');


var config = yaml.safeLoad(fs.readFileSync('build.yml', 'utf8'));
console.log(config);




gulp.task('vhost:add', function() {

  console.log(process.argv);

  for(var i=0;i<process.argv.length;i++){
    var option = process.argv[i];
    var args = option.split('=');
    switch(args[0])
    {
      case '--name':
        var name = args[1];
    }
  }


  shell.mkdir(config.apache.path_vhosts+'/'+name);
  shell.exec("echo 'vhost `"+name+"` successfully created, path: "+config.apache.path_vhosts+'/'+name+"' > "+config.apache.path_vhosts+'/'+name+"/index.html");

  var stream = gulp.src('./templates/vhost.twig')
      .pipe(twig({
          data: {
              vhost: name,
              benefits: [
                  'Fast',
                  'Flexible',
                  'Secure'
              ]
          }
      }))
      .pipe(dest(config.apache.path_conf, {ext: '.conf', basename:name}))
      .pipe(gulp.dest('/'))
      .on('end', function(){
        shell.exec("sudo service apache2 restart");
      });

});


gulp.task('compile', function () {

    return gulp.src('./index.twig')
        .pipe(twig({
            data: {
                title: 'Gulp and Twig',
                benefits: [
                    'Fast',
                    'Flexible',
                    'Secure'
                ]
            }
        }))
        .pipe(gulp.dest('./'));
});
